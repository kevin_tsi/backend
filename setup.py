from setuptools import setup, find_packages


def read_requirements(filename):
    return [req.strip() for req in open(filename)]


requirements = read_requirements('requirements.txt')
dev_requirements = read_requirements('requirements-dev.txt')


setup(
    name='wattsim',
    version=open('VERSION').read().strip(),
    author='Erwan Rouchet',
    author_email='lucidiot@protonmail.com',
    license='GNU General Public License v3 (GPLv3)',
    packages=find_packages(
        exclude=["*.tests", "*.tests.*", "tests.*", "tests"],
    ),
    package_data={
        '': ['*.md', 'LICENSE', 'README'],
    },
    python_requires='>=3.5',
    install_requires=requirements,
    extras_require={
        'dev': dev_requirements,
    },
    tests_require=dev_requirements,
    description='Backend for the WattSim project',
    long_description=open('README.rst').read(),
    long_description_content_type='text/x-rst',
    keywords='django arduino energy',
    url='https://gitlab.com/wattsim/backend',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Framework :: Django',
        'Framework :: Django :: 2.2',
        'Intended Audience :: Education',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Education',
    ],
    py_modules=['wattsim', ],
    scripts=[
        'wattsim/manage.py',
    ],
)
