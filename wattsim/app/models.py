from django.contrib.auth.models import AbstractUser
from django.db import models
from django.conf import settings
import uuid


class Station(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)


class Measurement(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    recorded = models.DateTimeField(auto_now_add=True)
    solar = models.FloatField(blank=True, null=True)
    wind = models.FloatField(blank=True, null=True)
    hydro = models.FloatField(blank=True, null=True)
    station = models.ForeignKey(Station, on_delete=models.CASCADE)

    class Meta:
        default_related_name = 'measurements'
        get_latest_by = 'recorded'
        ordering = ('recorded', )


# class User(AbstractUser):
#     is_teacher = models.BooleanField('teacher status', default=False)


# class Teacher(models.Model):
#     user = models.OneToOneField(User)


# class Classe(models.Model):
#     annee = models.CharField(max_length=9) # 2019/2020
#     nom_classe = models.CharField(max_length=10)
#     teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)


# class Student(models.Model):
#     user = models.OneToOneField(User)
#     classe = models.ForeignKey(Classe, on_delete=models.CASCADE)