from django.urls import path
from wattsim.app.views import UserAPIView, MeasurementAPIView, LatestMeasurementAPIView
from . import views
from django.contrib.auth import views as auth_views


app_name = 'wattsim.app'
urlpatterns = [
    path('user/', UserAPIView.as_view(), name='user'),
    path('measurements/latest/', LatestMeasurementAPIView.as_view(), name='latest-measurement'),
    path('measurements/', MeasurementAPIView.as_view(), name='measurements'),
    path('', views.home, name='home'),
    path('register/', views.register, name='register'),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='registration/logout.html'), name='logout'),
    path('profile/', views.profile, name='profile'),
]
