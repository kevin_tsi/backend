from django.contrib.auth import logout
from rest_framework.exceptions import NotFound
from rest_framework.generics import RetrieveAPIView, RetrieveDestroyAPIView, ListCreateAPIView
from rest_framework.mixins import CreateModelMixin
from rest_framework.permissions import IsAuthenticated
from wattsim.app.models import Measurement
from wattsim.app.serializers import UserSerializer, MeasurementSerializer
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from .forms import UserRegisterForm

class UserAPIView(CreateModelMixin, RetrieveDestroyAPIView):
    """
    Login, view the current user or logout.
    """
    name = 'Authentication'
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user

    def post(self, *args, **kwargs):
        return self.create(*args, **kwargs)

    def perform_destroy(self, instance):
        logout(self.request)


class MeasurementAPIView(ListCreateAPIView):
    """
    Browse the user's station's measurements or save a new measurement.
    """
    name = 'Measurements'
    serializer_class = MeasurementSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        return Measurement.objects.filter(station__user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(station=self.request.user.station)


class LatestMeasurementAPIView(RetrieveAPIView):
    name = 'Latest measurement'
    serializer_class = MeasurementSerializer
    permission_classes = (IsAuthenticated, )

    def get_object(self):
        try:
            return Measurement.objects \
                .filter(station__user=self.request.user) \
                .latest()
        except Measurement.DoesNotExist:
            raise NotFound


def home(request):
    """
    Renvoi vers la page home.html
    ou on visualise les données
    """
    measurement = Measurement.objects \
                .filter(station__user=request.user) \
                .latest()
    return render(request, 'app/home.html',{
        'title': 'Station',
        'measurement': measurement,
        })
  


def register(request):
    """
    Formulaire d'inscription d'un nouvelle utilisateur
    """
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'{username} votre compte est crée! Vous pouvez lancer votre maquette et vous connecter pour visualiser vos mesures.')
            return redirect('/login')
    else:
        form = UserRegisterForm()
    return render(request, 'app/register.html', {'form':form})

# messages.debug
# messages.info
# messages.success
# messages.warning
# messages.error


def profile(request):
    return render(request, 'app/profile.html')