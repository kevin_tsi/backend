Django>=2.1
djangorestframework>=3.10
django-cors-headers>=3
psycopg2-binary = "*"
django-crispy-forms = "*"
django-mathfilters = "*"